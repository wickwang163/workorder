<?php
namespace app\index\controller;

use app\common\controller\Base;
use think\Db;
use think\Request;
use think\Session;
use app\common\model\Task as TaskModel;
use app\common\model\UserHasGroup as UserHasGroupModel;

class Index extends Base
{
    protected function _initialize()
    {
        parent::_initialize();
    }

    public function index(Request $request, $page = 1)
    {
        if($this->current_user->is_admin){
            $tasks = TaskModel::order('create_time desc')->paginate($this->pagesize, false, ['page' => $page]);
        }else{
            $user_group_ids = UserHasGroupModel::where('user_id', $this->current_user->id)->column('group_id');
            $user_ids = UserHasGroupModel::whereIn('group_id', $user_group_ids)->column('user_id');
            $tasks = TaskModel::whereIn('user_id', $user_ids)->order('create_time desc')->paginate($this->pagesize, false, ['page' => $page]);
        }
        return view('index', ['tasks' => $tasks]);
    }

    public function create(Request $request){

        return view('create');

    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if($this->request->method() == 'POST'){
            $data = $this->request->param();
            $validate_result = $this->validate($data, 'Index');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if($data['discount'] === '' || is_null($data['discount'])){
                    $data['discount'] = 1;
                }
                if ($this->book_model->allowField(true)->save($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }

        }else{
            $this->error('保存失败');
        }
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $curl = new Curl();
        $disciplines = $curl->getDisciplines();
        $book_types = Type::all();
        $sizes = Size::all();
        $book = BookModel::find($id);
        if($book){
            return $this->fetch('partical_edit', [
                'book' => $book,
                'disciplines' => $disciplines,
                'book_types' => $book_types,
                'sizes' => $sizes
            ]);
        }else{
            $this->error('教材不存在');
        }
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        if($this->request->method() == 'PUT'){
            $data = $this->request->param();
            $validate_result = $this->validate($data, 'Index');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if($data['discount'] === '' || is_null($data['discount'])){
                    $data['discount'] = 1;
                }
                if ($this->book_model->allowField(true)->save($data, $id) !== false) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }

        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id, $ids = [])
    {
        if($this->request->method() == 'DELETE'){
            $id = $ids ? $ids : $id;
            if ($id) {
                if ($this->book_model->destroy($id)) {
                    $this->success('删除成功');
                } else {
                    $this->error('删除失败，教材不存在或者已经被使用');
                }
            } else {
                $this->error('请选择需要删除的教材');
            }

        }
    }

    public function downloadExcelForTextbook(){

        $phpExcel = $this->initPhpExcelData();

        $filename = '教材信息导入模板.xlsx';
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Disposition:inline;filename="'.$filename.'"');
        //生成excel文件
        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        //下载文件在浏览器窗口
        $objWriter->save('php://output');
        exit;
    }

    public function import(Request $request){

        if($this->request->method() == 'POST'){
            Db::transaction(function(){
                $file = request()->file('excel');
                if(!$file) {
                    $this->error('导入失败，文件不存在');
                }
                $filePath = $file->getRealPath();

                $objRead = new \PHPExcel_Reader_Excel2007();
                if(!$objRead->canRead($filePath)){
                    $objRead = new PHPExcel_Reader_Excel5();
                    if(!$objRead->canRead($filePath)){
                        $this->error('导入失败，文件不存在');
                    }
                }

                $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L');
                $fieldName = array(
                    'discipline_id',
                    'book_type_id',
                    'book_name',
                    'isbn',
                    'publisher',
                    'price',
                    'size_id',
                    'page',
                    'editor',
                    'publish_date',
                    'discount',
                    'comment'
                );
                $curl = new Curl();
                $obj = $objRead->load($filePath);  //建立excel对象
                $currSheet = $obj->getSheet(0);   //获取指定的sheet表
                $columnH = $currSheet->getHighestColumn();   //取得最大的列号
                $columnCnt = array_search($columnH, $cellName);
                $rowCnt = $currSheet->getHighestRow();   //获取总行数

                $data = array();
                for($_row=2; $_row<=$rowCnt; $_row++){  //读取内容
                    for($_column=0; $_column<=$columnCnt; $_column++){
                        $cellId = $cellName[$_column].$_row;
                        $cellValue = $currSheet->getCell($cellId)->getValue();
                        if($cellValue instanceof PHPExcel_RichText){   //富文本转换字符串
                            $cellValue = $cellValue->__toString();
                        }

                        if($fieldName[$_column] == 'discipline_id'){
                            $res = $curl->getDisciplineByName($cellValue);
                            $cellValue = !empty($res) ? $res['sj_id'] : '';
                        }elseif($fieldName[$_column] == 'book_type_id'){
                            $type = Type::where('name', $cellValue)->find();
                            $cellValue = $type ? $type->id : '';
                        }elseif($fieldName[$_column] == 'size_id'){
                            $size = Size::where('name', $cellValue)->find();
                            $cellValue = $size ? $size->id : '';
                        }elseif($fieldName[$_column] == 'discount'){
                            $cellValue = is_null($cellValue) || $cellValue === '' ? 1 : $cellValue;
                        }elseif($fieldName[$_column] == 'publish_date'){
                            $n = intval(($cellValue - 25569) * 3600 * 24); //转换成1970年以来的秒数
                            $cellValue = gmdate('Y-m-d',$n);//格式化时间,不是用date哦, 时区相差8小时的
                        }

                        $data[$fieldName[$_column]] = $cellValue;
                    }

                    $validate_result = $this->validate($data, 'Index');
                    if ($validate_result !== true) {
                        $this->error('文件中数据不符合规范，'.$validate_result.'，导入失败');
                    } else {
                        $book = new BookModel();
                        $book->allowField(true)->save($data);

                    }
                }

            });

            return $this->success('导入成功');

        }
    }

    public function export(){

        $phpExcel = $this->initPhpExcelData();

        $books = BookModel::all();
        for($i=0;$i<count($books);$i++){
            $book = isset($books[$i]) ? $books[$i] : null;
            if($book){
                $phpExcel->getActiveSheet()->setCellValue('A'.($i+2),$book->discipline_txt);
                $phpExcel->getActiveSheet()->setCellValue('B'.($i+2),$book->book_type_txt);
                $phpExcel->getActiveSheet()->setCellValue('C'.($i+2),$book->book_name);
                $phpExcel->getActiveSheet()->setCellValue('D'.($i+2),$book->isbn);
                $phpExcel->getActiveSheet()->setCellValue('E'.($i+2),$book->publisher);
                $phpExcel->getActiveSheet()->setCellValue('F'.($i+2),$book->price);
                $phpExcel->getActiveSheet()->setCellValue('G'.($i+2),$book->size_txt);
                $phpExcel->getActiveSheet()->setCellValue('H'.($i+2),$book->page);
                $phpExcel->getActiveSheet()->setCellValue('I'.($i+2),$book->editor);
                $phpExcel->getActiveSheet()->setCellValue('J'.($i+2),$book->publish_date);
                $phpExcel->getActiveSheet()->setCellValue('K'.($i+2),$book->discount);
                $phpExcel->getActiveSheet()->setCellValue('L'.($i+2),$book->comment);
            }
        }

        $filename = '教材信息数据.xlsx';
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Disposition:inline;filename="'.$filename.'"');
        //生成excel文件
        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        //下载文件在浏览器窗口
        $objWriter->save('php://output');
        exit;

    }

    private function initPhpExcelData(){
        $phpExcel = new \PHPExcel();
        $phpExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '所属学科')
            ->setCellValue('B1', '教材类型')
            ->setCellValue('C1', '教材名称')
            ->setCellValue('D1', 'ISBN')
            ->setCellValue('E1', '出版社')
            ->setCellValue('F1', '单价')
            ->setCellValue('G1', '开本')
            ->setCellValue('H1', '页码')
            ->setCellValue('I1', '主编')
            ->setCellValue('J1', '出版日期')
            ->setCellValue('K1', '折扣')
            ->setCellValue('L1', '备注');
        $phpExcel->getActiveSheet(0)->setTitle('教材基础信息');

        $phpExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('E')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('F')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('G')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('H')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('I')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('J')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('K')->setWidth(20);
        $phpExcel->setActiveSheetIndex(0)->getColumnDimension('L')->setWidth(20);

        return $phpExcel;
    }

}
