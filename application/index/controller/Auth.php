<?php

namespace app\index\controller;

use app\common\model\User;
use think\Controller;
use think\Request;
use think\Session;

class Auth extends Controller
{
    public function login(Request $request){
        if($request->method() == 'POST'){
            $data = $request->param();
            $validate_result = $this->validate($data, 'Auth');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $user = User::where(['username' => $data['username'], 'password' => md5($data['password'])])->find();
                if($user) {
                    Session::set("user_id",$user['id']);
                    Session::set("name",$user['name']);
                    Session::set("username",$user['username']);

                    return $this->success('登录成功,正在跳转...','index/index');

                } else {
                    //不存在该用户名
                    return $this->error('用户名或密码填写错误');
                }
            }

        }else{
            if(Session::has('user_id')){
                return $this->redirect('/');
            }else{
                return view('login');
            }
        }
    }

    public function logout(){
        Session::delete('user_id');
        Session::delete('name');
        Session::delete('username');
        return $this->redirect('/login');
    }
}