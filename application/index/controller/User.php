<?php

namespace app\index\controller;

use app\common\controller\Base;
use think\Controller;
use think\Request;
use app\common\model\User as UserModel;
use think\Validate;

class User extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request, $page = 1)
    {
        $users = UserModel::order('id desc')->paginate($this->pagesize, false, ['page' => $page]);
        return view('index', array(
            'users' => $users
        ));
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if($this->request->method() == 'POST'){

            $data = $request->param();
            $validate_result = $this->validate($data, 'User');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $user = new UserModel();
                $user->username = $data['username'];
                $user->name = $data['name'];
                $user->password = md5($data['password']);
                $user->is_admin = isset($data['is_admin']) && $data['is_admin'] == 1 ? 1 : 0;
                $res = $user->save();

                if ($res) {
                    $this->success('保存成功', '/user');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $user = UserModel::get($id);
        if(!$user){
            return $this->error('用户不存在', '/user');
        }
        return view('edit', ['user' => $user]);
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        if($this->request->method() == 'PUT'){
            $data = $request->param();
            $validate = new Validate([
                'username'   => 'require|unique:users',
                'name'   => 'require',
                'password'   => 'length:3,16',
                'password_confirm'   => 'length:3,16|confirm',
            ],[
                'username.require'   => '用户名必须填写',
                'username.unique'   => '该用户名已存在',
                'name.require'   => '姓名必须填写',
                'password.length'   => '密码长度范围为3~16',
                'password_confirm.length'   => '重复密码长度范围为3~16',
                'password_confirm.confirm'   => '两次密码输入不一致',
            ]);
            if (!$validate->check($data)) {
                $this->error($validate->getError());
            } else {
                $user = UserModel::find($id);
                if($user){
                    $user->username = $data['username'];
                    $user->name = $data['name'];
                    if(isset($data['is_admin']) && $data['is_admin'] == 1){
                        $user->is_admin = 1;
                    }else{
                        $user->is_admin = 0;
                    }
                    if(isset($data['password']) && $data['password'] != ''){
                        $user->password = md5($data['password']);
                    }
                    $user->save();

                    return $this->success('保存成功', '/user');
                }else{
                    $this->error('用户不存在');
                }
            }

        }else{
            $this->error('保存失败');
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if($this->request->method() == 'DELETE'){

            $user = UserModel::get($id);
            if($user && $user->id != 1){    //管理员无法删除
                $res = $user->delete();
                if($res){
                    $this->success('删除成功');
                }else{
                    $this->error('删除失败');
                }
            }else {
                $this->error('删除失败');
            }

        }
    }
}
