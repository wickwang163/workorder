<?php

namespace app\index\controller;

use app\common\controller\Base;
use think\Db;
use think\Request;
use app\common\model\Group as GroupModel;
use app\common\model\User as UserModel;
use app\common\model\UserHasGroup as UserHasGroupModel;

class Group extends Base
{
    protected function _initialize()
    {
        parent::_initialize();
        if(!$this->isAdmin()){
            $this->error('您没有权限访问此页面', '/');
        }
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request, $page = 1)
    {
        $groups = GroupModel::order('create_time desc')->paginate($this->pagesize, false, ['page' => $page]);
        return view('index', array(
            'groups' => $groups
        ));
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $users = UserModel::all();
        return view('create', ['users' => $users]);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if($this->request->method() == 'POST'){

            $data = $request->param();
            $validate_result = $this->validate($data, 'Group');
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                Db::transaction(function()use($data){
                    $group = new GroupModel();
                    $group->name = $data['name'];
                    $group->created_by_user_id = $this->current_user->id;
                    $res = $group->save();

                    if ($res) {
                        if(isset($data['users'])){
                            foreach ($data['users'] as $user_id){
                                $user = UserModel::find($user_id);
                                if($user){
                                    $user_has_group = new UserHasGroupModel();
                                    $user_has_group->user_id = $user_id;
                                    $user_has_group->group_id = $group->id;
                                    $user_has_group->save();
                                }
                            }
                        }
                    } else {
                        $this->error('保存失败');
                    }
                });

                $this->success('保存成功', '/group');
            }
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $group = GroupModel::get($id);
        if(!$group){
            return $this->error('团队不存在', '/group');
        }
        $users = UserModel::all();
        $user_has_groups = UserHasGroupModel::where('group_id', $id)->column('user_id');
        return view('edit', ['group' => $group, 'users' => $users, 'user_has_groups' => $user_has_groups]);
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        if($this->request->method() == 'PUT'){
            $data = $request->param();
            $validate_result = $this->validate($data, 'Group');
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                Db::transaction(function()use($id, $data){
                    $group = GroupModel::find($id);
                    if($group){
                        $group->name = $data['name'];
                        $group->save();

                        $old_user_has_groups = UserHasGroupModel::where('group_id', $id)->column('user_id');
                        $new_user_has_groups = $data['users'];
                        $del_data = array_diff($old_user_has_groups, $new_user_has_groups);
                        $add_data = array_diff($new_user_has_groups, $old_user_has_groups);

                        UserHasGroupModel::where('group_id', $id)->whereIn('user_id', $del_data)->delete();
                        foreach ($add_data as $val){
                            $user_has_group = new UserHasGroupModel();
                            $user_has_group->group_id = $id;
                            $user_has_group->user_id = $val;
                            $user_has_group->save();
                        }

                    }else{
                        $this->error('团队不存在');
                    }
                });

                return $this->success('保存成功', '/group');

            }

        }else{
            $this->error('保存失败');
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if($this->request->method() == 'DELETE'){

            $group = GroupModel::get($id);
            if($group){
                $res = $group->delete();
                if($res){
                    $this->success('删除成功');
                }else{
                    $this->error('删除失败');
                }
            }else {
                $this->error('删除失败');
            }

        }
    }
}
