<?php

namespace app\index\controller;

use app\common\controller\Base;
use app\common\model\TaskReply;
use think\Controller;
use think\Request;
use app\common\model\User as UserModel;
use app\common\model\Task as TaskModel;
use app\common\model\TaskReply as TaskReplyModel;

class Task extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $users = UserModel::all();
        return view('create', ['users' => $users]);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if($this->request->method() == 'POST'){

            $data = $request->param();
            $validate_result = $this->validate($data, 'Task');
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {

                if(isset($data['user_id']) && $data['user_id'] != ''){
                    $user = UserModel::find($data['user_id']);
                    if(!$user){
                        $this->error('指派的用户不存在');
                    }
                }
                $task = new TaskModel();
                $task->title = $data['title'];
                $task->desc = $data['desc'];
                $task->created_by_user_id = $this->current_user->id;
                if(isset($data['user_id'])) {
                    $task->user_id = $data['user_id'];
                }
                if(isset($data['type']) && isset($this->task_types[$data['type']])) {
                    $task->type = $data['type'];
                }
                $task->expected_hours = $data['expected_hours'];
                if(isset($data['expected_date'])) {
                    $date_arr = explode(' - ', $data['expected_date']);
                    if(count($date_arr) == 2){
                        $task->start_date = $date_arr[0];
                        $task->end_date = $date_arr[1];
                    }
                }
                $task->save();
                $this->success('保存成功', '/');
            }
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $task = TaskModel::get($id);
        if(!$task){
            return $this->error('任务不存在', '/');
        }
        if(!($this->current_user->id == 1 || $task->user_id == $this->current_user->id)){
            return $this->error('您没有权限处理该任务', '/');
        }
        $task_replys = TaskReplyModel::where('task_id', $id)->select();
        $users = UserModel::all();
        if($task->status == 0){
            $task->status = 1;
            $task->save();
        }

        return view('edit', ['task' => $task, 'task_replys' => $task_replys, 'users' => $users]);
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        if($this->request->method() == 'PUT'){
            $task = TaskModel::get($id);
            if(!$task){
                return $this->error('任务不存在', '/');
            }
            if(!($this->current_user->id == 1 || $task->user_id == $this->current_user->id)){
                return $this->error('您没有权限处理该任务', '/');
            }
            $data = $request->param();
            $validate_result = $this->validate($data, 'TaskReply');
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $task_reply = new TaskReplyModel();
                $task_reply->task_id = $id;
                $task_reply->content = $data['desc'];
                $task_reply->created_by_user_id = $this->current_user->id;
                $task_reply->resolution = $data['resolution'];
                $task_reply->actual_hours = $data['actual_hours'];
                $task_reply->resolution_date = $data['resolution_date'];
                $task_reply->save();

                $task->user_id = $data['user_id'];
                if($data['resolution'] == 3){
                    $task->status = 2;
                }
                $task->save();

                return $this->success('保存成功', '/');
            }

        }else{
            $this->error('保存失败');
        }

    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
