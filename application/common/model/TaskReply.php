<?php
namespace app\common\model;

use think\Model;

class TaskReply extends Model
{
    protected $table = 'task_replys';

    protected $autoWriteTimestamp = true;

    public function getTypeTxtAttr($value, $data){
        $task_reply_types = [
            '0' => '设计如此',
            '1' => '重复Bug',
            '2' => '外部原因',
            '3' => '已解决',
            '4' => '无法重现',
            '5' => '延期处理',
            '6' => '不予解决',
        ];

        return isset($task_reply_types[$data['type']]) ? $task_reply_types[$data['type']] : '';
    }

    public function task()
    {
        return $this->hasOne('Task', 'id', 'task_id')->find();
    }

    public function user()
    {
        return $this->hasOne('User', 'id', 'created_by_user_id')->find();
    }

}