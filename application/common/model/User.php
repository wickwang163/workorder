<?php
namespace app\common\model;

use think\Model;

class User extends Model
{
    protected $table = 'users';

    protected $autoWriteTimestamp = false;

    public function  getIsAdminTxtAttr($value, $data){
        return $data['is_admin'] == 1 ? '是' : '否';
    }

}