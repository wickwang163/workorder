<?php
namespace app\common\model;

use think\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $autoWriteTimestamp = true;

    public function getTypeTxtAttr($value, $data){
        $task_types = [
            '0' => '设计',
            '1' => '开发',
            '2' => '测试',
            '3' => '研究',
            '4' => '问题',
            '5' => 'Bug',
            '6' => '其他',
        ];

        return isset($task_types[$data['type']]) ? $task_types[$data['type']] : '';
    }

    public function user()
    {
        return $this->hasOne('User', 'id', 'user_id')->find();
    }

    public function createdByuser()
    {
        return $this->hasOne('User', 'id', 'created_by_user_id')->find();
    }

    public function getUserTxtAttr($value, $data){
        $user = $this->user();
        return $user ? $user['name'] : '';
    }

    public function getCreatedByUserTxtAttr($value, $data){
        $user = $this->createdByuser();
        return $user ? $user['name'] : '';
    }

    public function getStatusTxtAttr($value, $data){
        switch ($data['status']){
            case 0:
                $res = '待处理';
                break;
            case 1:
                $res = '已查看';
                break;
            case 2:
                $res = '已完成';
                break;
            default:
                $res = '待处理';
                break;

        }
        return $res;
    }

    public function getExpectedDateAttr($value, $data){
        if($data['start_date'] && $data['end_date']){
            return $data['start_date'] . ' - ' . $data['end_date'];
        }else{
            return '';
        }

    }
}