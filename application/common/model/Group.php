<?php
namespace app\common\model;

use think\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $autoWriteTimestamp = true;

    public function getCreatedUserTxtAttr($value, $data){
        $user = User::find($data['created_by_user_id']);
        $res = $user ? $user['name'] : '';

        return $res;
    }

}