<?php
namespace app\common\model;

use curl\Curl;
use think\Model;

class UserHasGroup extends Model
{
    protected $table = 'user_has_groups';

    protected $autoWriteTimestamp = false;

    public function user()
    {
        return $this->hasOne('User', 'id', 'user_id')->find();
    }

    public function group()
    {
        return $this->hasOne('Group', 'id', 'group_id')->find();
    }

}