<?php
namespace app\common\validate;

use think\Validate;

class Task extends Validate
{
    protected $rule = [
        'title'   => 'require|max:255',
        'desc'   => 'require',
        'expected_hours'   => 'number',
        'user_id' => 'require'
    ];

    protected $message = [
        'title.require'   => '标题必须填写',
        'title.max'   => '标题长度过长',
        'expected_hours.number'   => '预计工时必须是数字',
        'desc.require'   => '描述必须填写',
        'user_id.require'   => '指派用户必须选择',
    ];
}