<?php
namespace app\common\validate;

use think\Validate;

class TaskReply extends Validate
{
    protected $rule = [
        'resolution'   => 'require',
        'actual_hours'   => 'number',
        'user_id' => 'require'
    ];

    protected $message = [
        'resolution.require'   => '解决方案必须选择',
        'user_id.require'   => '指派用户必须选择',
        'actual_hours.number'   => '实际耗时必须是数字',
    ];
}