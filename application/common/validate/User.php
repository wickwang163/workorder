<?php
namespace app\common\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'username'   => 'require|unique:users',
        'name'   => 'require',
        'password'   => 'require|length:3,16',
        'password_confirm'   => 'require|length:3,16|confirm',
    ];

    protected $message = [
        'username.require'   => '用户名必须填写',
        'username.unique'   => '该用户名已存在',
        'name.require'   => '姓名必须填写',
        'password.require'   => '密码必须填写',
        'password.length'   => '密码长度范围为3~16',
        'password_confirm.require'   => '重复密码必须填写',
        'password_confirm.length'   => '重复密码长度范围为3~16',
        'password_confirm.confirm'   => '两次密码输入不一致',
    ];
}