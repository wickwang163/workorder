<?php
namespace app\common\validate;

use think\Validate;

class Auth extends Validate
{
    protected $rule = [
        'username'   => 'require',
        'password'   => 'require|length:3,16',
    ];

    protected $message = [
        'username.require'   => '用户名必须填写',
        'password.require'   => '密码必须填写',
        'password.length'   => '密码长度范围为3~16',
    ];
}