<?php
namespace app\common\validate;

use think\Validate;

class Index extends Validate
{
    protected $rule = [
        'discipline_id'   => 'require',
        'book_type_id'   => 'require',
        'book_name'   => 'require',
        'isbn'   => 'require|number|length:10|unique:books',
        'publisher'   => 'require',
        'price'   => 'require|number',
        'discount'   => 'number|between:0,1',
    ];

    protected $message = [
        'discipline_id.require'   => '所属学科必须填写',
        'book_type_id.require'   => '教材类型必须填写',
        'book_name.require'   => '教材类型名称必须填写',
        'isbn.require'   => 'isbn必须填写',
        'isbn.number'   => 'isbn必须是数字',
        'isbn.length'   => 'isbn必须是10位数字',
        'isbn.unique'   => '该isbn已存在',
        'publisher.require'   => '出版社必须填写',
        'price.require'   => '价格必须填写',
        'price.number'   => '价格必须是数字',
        'discount.number'   => '折扣必须是数字',
        'discount.between'   => '折扣范围为0~1',
    ];
}