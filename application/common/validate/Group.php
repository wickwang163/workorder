<?php
namespace app\common\validate;

use think\Validate;

class Group extends Validate
{
    protected $rule = [
        'name'   => 'require|unique:groups|max:50',
    ];

    protected $message = [
        'name.require'   => '团队名必须填写',
        'name.unique'   => '该团队名已存在',
        'name.max'   => '团队名长度过长',
    ];
}