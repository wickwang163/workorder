<?php

namespace app\common\controller;

use app\common\model\User;
use think\Controller;
use think\Session;

class Base extends Controller
{
    protected $pagesize = 10;
    protected $current_user;
    protected $task_types = [
        '0' => '设计',
        '1' => '开发',
        '2' => '测试',
        '3' => '研究',
        '4' => '问题',
        '5' => 'Bug',
        '6' => '其他',
    ];
    protected $task_reply_types = [
        '0' => '设计如此',
        '1' => '重复Bug',
        '2' => '外部原因',
        '3' => '已解决',
        '4' => '无法重现',
        '5' => '延期处理',
        '6' => '不予解决',
    ];

    protected function _initialize()
    {
        parent::_initialize();

        if(!Session::has('user_id')) {
            $this->redirect('/login');
        }

        $this->current_user = User::find(Session::get('user_id'));
        $this->view->assign('user_id',Session::get('user_id'));
        $this->view->assign('name',Session::get('name'));
        $this->view->assign('current_user',$this->current_user);
        $this->view->assign('controller', $this->request->controller());
        $this->view->assign('action', $this->request->action());
        $this->view->assign('task_types', $this->task_types);
        $this->view->assign('task_reply_types', $this->task_reply_types);
    }

    public function isAdmin(){
        return $this->current_user->is_admin ? true : false;
    }

}