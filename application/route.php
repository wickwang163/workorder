<?php

use think\Route;

/**
 *
 * Route::rule('/abcd','index/index/index', 'GET');
 * Route::get('new/:id','News/read'); // 定义GET请求路由规则
 * Route::post('new/:id','News/update'); // 定义POST请求路由规则
 * Route::put('new/:id','News/update'); // 定义PUT请求路由规则
 * Route::delete('new/:id','News/delete'); // 定义DELETE请求路由规则
 * Route::any('new/:id','News/read'); // 所有请求都支持的路由规则
 * Route::rule('new/:id','News/read','GET|POST');   //GET或者POST请求路由规则
 * Route::resource('blog','index/blog'); //资源路由
 * 资源路由含有7个规则 分别是index create save read edit update delete
 * Route::group('blog',[
 * ':id'   => ['Blog/read', ['method' => 'get'], ['id' => '\d+']],
 * ':name' => ['Blog/read', ['method' => 'post']],
 * ],['method'=>'get','ext'=>'html']);
 *
 */

Route::get('/','index/index');
Route::rule('/login','Auth/login', 'GET|POST');
Route::get('/logout','Auth/logout');
Route::resource('group','group');
Route::resource('user','user');
Route::resource('task','task');


Route::rule('textbook/download_excel_for_textbook','index/downloadExcelForTextbook','GET');
Route::rule('textbook/import','index/import','POST');
Route::rule('textbook/export','index/export','GET');
Route::resource('textbook','index');

Route::rule('/order/multi_check','order/multiCheck','POST');
Route::rule('/order/check/:id','order/check','POST|GET');
Route::rule('/order/print/:id','order/orderPrint','GET');
Route::rule('order/download_excel_for_order','order/downloadExcelForOrder','GET');
Route::rule('order/import','order/import','POST');
Route::rule('order/export','order/export','GET');
Route::resource('order','order');

Route::rule('/instorage/multi_check','instorage/multiCheck','POST');
Route::rule('/instorage/check/:id','instorage/check','POST|GET');
Route::rule('/instorage/print/:id','instorage/instoragePrint','GET');
Route::rule('instorage/download_excel_for_instorage','instorage/downloadExcelForInstorage','GET');
Route::rule('instorage/import','instorage/import','POST');
Route::rule('instorage/export','instorage/export','GET');
Route::rule('/instorage/init','instorage/init','POST|GET');
Route::resource('instorage','instorage');

Route::rule('outstorage/get_store_by_book_id/:id','outstorage/getStoreByBookId','GET');
Route::rule('outstorage/print/:id','outstorage/outstoragePrint','GET');
Route::rule('outstorage/multi_check','outstorage/multiCheck','POST');
Route::rule('outstorage/check/:id','outstorage/check','POST|GET');
Route::rule('outstorage/download_excel_for_outstorage','outstorage/downloadExcelForOutstorage','GET');
Route::rule('outstorage/import','outstorage/import','POST');
Route::rule('outstorage/export','outstorage/export','GET');
Route::resource('outstorage','outstorage');


Route::get('disposition/handle_list','disposition/handleList');
Route::get('disposition/get_store_by_book_id/:id','disposition/getStoreByBookId');
Route::rule('disposition/print/:id','disposition/dispositionPrint','GET');
Route::rule('disposition/multi_check','disposition/multiCheck','POST');
Route::rule('disposition/check/:id','disposition/check','POST|GET');
Route::rule('disposition/download_excel_for_disposition','disposition/downloadExcelForDisposition','GET');
Route::rule('disposition/import','disposition/import','POST');
Route::rule('disposition/export','disposition/export','GET');
Route::resource('disposition','disposition');


Route::get('basis','basis/index');
Route::post('basis/init','basis/init');
Route::get('store_manager','store/index');
Route::rule('store_manager/save','store/save','POST');
Route::get('store_manager/edit','store/edit');
Route::rule('store_manager/update','store/update','POST');
Route::rule('store_manager/delete','store/delete','DELETE');
Route::resource('type','type');
Route::resource('size','size');
