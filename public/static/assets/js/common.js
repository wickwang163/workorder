/**
 * 操作平台JS主入口
 */

$(function () {
    ajaxPostForm = function (_form) {
        $.ajax({
            url: _form.attr('action'),
            type: _form.attr('method'),
            data: _form.serialize(),
            dateType: 'json',
            success: function (info) {
                if (info.status) {
                    if(info.url != ''){
                        setTimeout(function () {
                            location.href = info.url;
                        }, 1000);
                    }
                    setTimeout(function () {
                        layer.closeAll();
                    }, 1000);
                }else{
                    if(info.error_fields){
                        for (var i = 0; i < info.error_fields.length; i++) {
                            $('#'+info.error_fields[i]).addClass('has-error').parent().prev().addClass('has-error');
                        }
                    }
                }
                layer.msg(info.msg);

            }
        });
    }

    initLayuiDate = function (elem) {

        layui.use('laydate', function () {
            var laydate = layui.laydate;
            // 日历组件
            laydate.render({
                elem: elem //指定元素
            });
        });
    }


    /**
     * 通用全选
     */
    $('.check-all').on('click', function () {
        $(this).parents('table').find('input[type="checkbox"]').prop('checked', $(this).prop('checked'));
    });

    /**
     * 通用批量处理（审核、取消审核、删除）
     */
    $('.ajax-multi-delete').on('click', function () {
        var _action = $(this).data('action');
        layer.open({
            shade: false,
            content: '确定删除？',
            btn: ['确定', '取消'],
            yes: function (index) {
                $.ajax({
                    url: _action,
                    type: 'DELETE',
                    data: $('.ajax-form').serialize(),
                    success: function (info) {
                        if (info.code === 1) {
                            setTimeout(function () {
                                location.href = info.url;
                            }, 1000);
                        }
                        layer.msg(info.msg);
                    }
                });
                layer.close(index);
            }
        });

        return false;
    });

    $(document).on('click', ".deleteBtn", function (e) {
        var id = $(this).data('id');
        var url = $(this).data('url');
        layer.confirm('确认删除吗？', {
            title: '删除',
            btn: ['确认', '取消'] //可以无限个按钮
        }, function(index, layero){
            $.ajax({
                url: url,
                type: 'DELETE',
                data: {id: id},
                dateType: 'json',
                success: function (info) {
                    if (info.code === 1) {
                        setTimeout(function () {
                            location.href = info.url;
                        }, 1000);
                    }
                    layer.msg(info.msg);

                }
            });
        }, function(index){
            layer.closeAll();
        });

    })


})

/**
 * 操作平台JS主入口
 */

layui.define(['form', 'layer'], function (exports) {
    var layer = layui.layer,
        desc = '',
        form = layui.form;


    /**
     * AJAX全局设置
     */
    $.ajaxSetup({
        type: "post",
        dataType: "json"
    });

    //自定义验证规则
    form.verify({
        title: function(value){
            if(value.length < 5){
                return '标题至少得5个字符啊';
            }
        },
        password: [/(.+){3,16}$/, '密码必须6到20位'],
        content: function(value){
            for (instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
            desc = $("#desc").val();
        }
    });

    /**
     * 通用表单提交(AJAX方式)
     */
    form.on('submit(*)', function (data) {
        //同步获取desc的值
        data.field.desc = desc;
        $.ajax({
            url: data.form.action,
            type: data.form.method,
            data: data.field,
            success: function (info) {
                if (info.code === 1) {
                    setTimeout(function () {
                        location.href = info.url;
                    }, 1000);
                }
                layer.msg(info.msg);

            }
        });

        return false;
    });

    exports('common', {});
});
